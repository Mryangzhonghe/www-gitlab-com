---
layout: markdown_page
title: "Simplified DevOps Maturity Model"
---

## On this page
{:.no_toc}

- TOC
{:toc}

As organizations embark on adopting DevOps practices, they typically adopt a series of DevOps practices and improve along a maturity curve, where specific practices and processes are improved first, followed by others. No organization goes from zero to fully mature DevOps in one step, it's a journey of continuous improvment. 

Typical maturity models are complex, multidimensional constructs which are used to measure maturity, plan improvements and in some cases document the organization's effectiveness. The traditional 5 level model (Initial, Managed, Defined, Quantitatively Managed, Optimizing) is perhaps overly complex for describing the maturity of an organization on their DevOps Adoption.

We can define a simpler model, something similar to this three-level model for Continuous Delivery Maturity: 
[http://bekkopen.github.io/maturity-model/](http://bekkopen.github.io/maturity-model/)

This simplified DevOps Maturity Model descibes a typical sequence of evolving DevOps maturity where the focus started with improving development practices, then focuses on how they deploy and release consistently, finally focusing on how the organization optimizes the end-to-end lifecycle.

## Simplified DevOps Maturity Model 

| **Adopting**   |  **Scaling**   |  **Optimizing** |
|----------|-------------|------|
| Starting to adopt DevOps, development teams focus on their core software development practices to streamline and accelerate their ability to organize and create software. Specifically, they focus on adopting modern development practices, source code management, automating build and testing to make their work more consistent and responsive to change. | Building on stable, iterative, and repeatable development practices, teams scale and extend these practices to address the challenges of delivering code to production, bridging the gap with automated configuration, deployment, and release of applications to end users. | With a maturing DevOps lifecycle, teams focus on optimizing their business value, leveraging end-to-end feedback, monitoring, and insight to streamline and improve DevOps practices. They identify and address constraints, such as security testing, where they tune and improve their processes increasing velocity, quality, and security. |