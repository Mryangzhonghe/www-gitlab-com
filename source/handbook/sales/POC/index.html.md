---
layout: markdown_page
title: "GitLab POC Template"
---

# Guided Proof of Concept (POC) Guidelines

GitLab wants prospects to enjoy a successful Proof of Concept with GitLab Enterprise Edition. A POC is a collaboration between GitLab and the prospective customer for evaluating GitLab Enterprise Edition.  

The target duration for a POC is 10 business days, extended to a total duration of not more than 20 business days. A GitLab Customer Success collaborative project should be utilized as the default method to manage a POC, while a [document](#POCdoc) is available when using a GitLab.com project is not an option.

## Key Players

### Customer  

*  Executive contact - Someone with business level, budgetary buy in
*  At least one pilot team to execute the POC
*  Technical POC lead

### GitLab Role / Responsibilities

*  Strategic Account Leader (SAL) - Relationship manager, owns temporary license, POC kickoff, SFDC admin, scheduling
*  Solutions Architect (SA) - Primary technical contact, POC lead
*  Technical Account Manager (TAM) - Project management; Ensures all tasks in playbook are done on time; included in communication and calls for visibility
*  Professional Services - On-hand if needed, include for visibility only
*  Support - On-hand if needed

*Note: in the case of a "lite" POC, the Solutions Architect is expected to be the sole GitLab contact. "Lite" is determined case-by-case by the size of the prospect as well as their ability to engage with GitLab.*

## POC Meetings and Tasks

### POC Kickoff Checklist

* [ ] SA: Ensure the customer architecture is prepared to support an on-premises POC
* [ ] SA: Ensure customer network has access to GitLab.com
* [ ] SA: Customer Success project is created in GitLab as per the [handbook](/handbook/customer-success/tam/#to-start-a-new-customer-engagement)
* [ ] SA: POC document is created if this is required by the customer, otherwise default to the Customer Success project
* [ ] SA: Ensure POC goals and business outcomes are clearly identified prior to kickoff
* [ ] SA: Notify GitLab Support of POC dates, customer, and other relevant information
* [ ] SAL: Opportunity updated in Salesforce, set to Stage 3-Technical Evaluation, with POC Information entered per the [handbook](/handbook/business-ops/#opportunity-stages)
* [ ] SAL: Signed NDA by the legal team if required
* [ ] SAL: Schedule Internal kick off meeting (detailed below)
* [ ] SAL: Schedule kickoff meeting with customer
* [ ] TAM: Review collaborative project content prior to internal kickoff meeting

### Meeting Cadence

Calls may be recorded with customer consent, and recordings may be stored in the Recorded Meetings folder in the project repository for mutual benefit.  

### Internal Kickoff Meeting - 1 - 1.5 hours, led by the Solutions Architect

#### Attendees

*  Strategic Account Leader
*  Solutions Architect
*  Technical Account Manager

#### Agenda

*  Collaboratively review customer success project README to ensure everything is correct
*  POC document review (only if a document is required for the POC)
*  Discussion of strategy, whether GitLab Support or Professional Services need to be notified/included in the POC
*  Strategic Account Leader to schedule external kickoff with customer

### External Kickoff Meeting (Remote) - 1 hour, led by the Solutions Architect

#### Attendees

*  Strategic Account Leader
*  Solutions Architect
*  Technical Account Manager
*  Professional Services (if required)
*  Customer Executive contact
*  Customer Technical POC lead

#### Agenda

*  Screen share with the the customer to discuss the below agenda items
*  Collaboratively review customer success project README to ensure everything is correct
*  Agree on due dates for POC completion, add to project POC milestone
*  Review project POC issues - add new issues if necessary with agreed due dates
*  Review POC document with customer (only when required)
*  Establish cadence with customer, Strategic Account Leader to schedule DURING this call
*  Create issues for each cadence call with customer under the POC milestone for call notes
*  Provision licenses and establish if customer needs help getting GitLab set up and configured

### Week One Retrospective call, led by the Solutions Architect

#### Attendees

*  Strategic Account Leader (visibility only)
*  Solutions Architect
*  Technical Account Manager (visibility only)
*  Professional Services (if required)
*  Customer's Technical POC Lead

#### Agenda

* What went well?
* What went wrong?
* What do we need to change?
* Review success criteria - are we on track?

### POC Wrap Up Meeting - Led by the Strategic Account Leader

#### Attendees

*  Strategic Account Leader
*  Solutions Architect
*  Technical Account Manager
*  Professional Services (if required)
*  Support (if required)
*  Customer Executive contact
*  Customer Technical POC Lead

#### Agenda

* Did we meet the success criteria?
* Did we meet the goals for the customer? Is this a technical win?
* Next steps
* Send out POC survey feedback form

## POC Calendar

### Week one

*  Work with customer to setup and configure GitLab if required
*  Intro to GitLab via demo covering areas specific to customer needs
*  Conduct cadence calls as agreed in kickoff meeting

### Week two

* Week One Retrospective (Monday)
* End of POC cadence call (Friday)

## <a name="POCdoc"></a>Proof of Concept (POC) Template Document

As an alternative (or in addition) to using a collaborative GitLab project, a document is available which helps outline the details of a POC. In the [document](https://docs.google.com/document/d/1anbNik_H_XDHJYyZPkfr4_6wvIXgqbwvWynN9v9tj2E/edit#) (only accessible to GitLab team members), provides the framework for a successful POC by addressing current state issues, persistent challenges, business problems, desired state and outcomes.  

This document suggests and verifies specific success criteria for any POC, as well as outlining a mutual commitment between GitLab and the identified prospect parties. It also specifies the limited timeframe in which the POC will occur.  

### Using the Proof of Concept (POC) Template

The template provides a standardized approach to the POC process, but the document requires customization for each and every POC to be executed.  

To use the template, begin by making a copy of the master document for each POC.  

Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location, and the client pilot team(s).  

After all the highlighted sections have been completely filled in, collaboratively complete the **Date** and **Owner** column fields within the **Project Plan** and **Roles and Responsibilities** sections.  

Finally, ensure both GitLab and the prospect have a copy of the document. Schedule all meetings associated to the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.  
