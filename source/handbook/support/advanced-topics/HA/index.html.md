---
layout: markdown_page
title: Checklist for becoming a High Availability Expert
---

Create an issue with this checklist on the [support team issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/)
with the **Title:** *"Gitlab HA Boot Camp - your_name"*

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

```
**Goal of this checklist:** Set a clear path for High Availability Expert training

### Stage 1: Commit and Become familiar with what High Availability is

- [ ] **Done with Stage 1**

  1. [ ] Ping your manager on the issue to notify him you have started
  1. [ ] In your Slack Notification Settings, set **HA** as a **Highlight Word**
  1. [ ] Commit to this by notifying the current experts that they can start routing non-technical HA questions to you
  1. [ ] Understand the [basic concept of high availability](https://www.digitalocean.com/community/tutorials/what-is-high-availability)
  1. [ ] Understand the use of [GitLab High Availability roles](https://docs.gitlab.com/omnibus/roles/)
  1. [ ] Understand the different [HA architectures possible with GitLab](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/3647/diffs)
    
### Stage 2: Technical Setup

- [ ] **Done with Stage 2**
    
  1. [ ] Set up a minimal HA setup using the GitLab omnibus package following the documentation at https://docs.gitlab.com/ee/administration/high_availability/
  1. [ ] [Configure database](https://docs.gitlab.com/ee/administration/high_availability/database.html)
  1. [ ] [Configure Redis](https://docs.gitlab.com/ee/administration/high_availability/redis.html)
  1. [ ] [Configure NFS](https://docs.gitlab.com/ee/administration/high_availability/nfs.html)
  1. [ ] [Configure GitLab Application servers](https://docs.gitlab.com/ee/administration/high_availability/gitlab.html)
  1. [ ] [Configure load balancers](https://docs.gitlab.com/ee/administration/high_availability/load_balancer.html)
    
    ** A minimal setup has 2 application servers, 7 nodes for database components and 1 load balancer.
    
### Stage 3: GitLab HA Administration

- [ ] **Done with Stage 3**
    


Remember to contribute to any documentation that needs updating

### Stage 4: Tickets

- [ ] **Done with Stage 4**

- [ ] Contribute valuable responses on at least 10 HA tickets. Even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay it to
the customer.

  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __

### Stage 5: Customer Calls


### [ ] Stage 5: Quiz?


### Final Stage

- [ ] Your Manager needs to check this box to acknowledge that you finished
- [ ] Send a MR to declare yourself an **HA Expert** on the team page
```