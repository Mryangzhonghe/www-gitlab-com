---
layout: markdown_page
title: "Team Structure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organizational chart

You can see who reports to whom on our [organizational chart](/team/chart).

## Table

| Layer | Example | Management group | Reporting group |
|--------------------------------|---------------------------------------|------------------------------------------------------------|-----------------|
| Board member | [Chief Executive Officer](/roles/chief-executive-officer/) | Board | Company |
| CxO and VP | [Chief Culture Officer](/roles/people-ops/chief-culture-officer/) | C-group or Executive group (incl. VP's) | Function |
| Director | [Director of Engineering](/roles/engineering/engineering-management/#director-of-engineering) | Director group | Department |
| Manager | [Engineering Manager](/roles/engineering/engineering-management/#engineering-manager) | Management group | Team |
| Individual contributor (IC) | [Staff Developer](/roles/engineering/developer/#staff-developer) | Company | Contributors |
| Community member | [Most Valuable Person](/mvp/) | Community | Wider community |

The management group includes the roles above. The reporting group includes the roles below, although sometimes without the community member layer.

## Layers

GitLab Inc. has at most five layers in the company structure (IC up to CEO). There is one exception to the above structure. Due to the large number of sales development representatives (SDRs) and business development representatives (BDRs) we have 1 extra layer of management: team leads that report into the manager of sales and business development.

Some of individual contributors (without any direct reports) have manager in their title but are not considered a manager in our company structure structure nor salary calculator, examples are product manager, accounting manager, account manager, channel sales manager, technical account manager, field marketing managers, online marketing manager, and product marketing manager.

## Specialists, experts, and mentors

People can be a specialist in one thing and be an expert in multiple things. These are listed on the [team page](/team/).

### Specialist

Specialists carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Sometimes there is a lead in this topic that they report to.
You can be a specialist in only one topic.
The specialist description is a paragraph in the job description for a certain title.
A specialist is listed after a title, for example: Developer, database specialist (do not shorten it to Developer, database).
The if you can have multiple ones and/or if you don't spend the majority of your time there it is probably an [expertise](/team/structure/#expert).
Since a specialist has the same job description as others with the title they have the same career path and compensation.

### Expert

Expert means you have above average experience with a certain topic.
Commonly, you're expert in multiple topics after working at GitLab for some time.
This helps people in the company to quickly find someone who knows more.
Please add these labels to yourself and assign the merge request to your manager.
An expertise is not listed in a role description, unlike a [specialist](/roles/specialist).

For Production Engineers, a listing as "Expert" can also mean that the individual
is actively [embedded with](/handbook/infrastructure/#embedded) another team.
Following the period of being embedded, they are experts in the regular sense
of the word described above.

Developers focused on Reliability and Production Readiness are named [Reliability Expert](/roles/expert/reliability/).

### Mentor

Whereas an expert might assist you with an individual issue or problem, mentorship is about helping someone grow their career, functional skills, and/or soft skills. It's an investment in someone else's growth.

Some people think of expertise as hard skills (Ruby, International Employment Law, etc) rather than soft skills (managing through conflict, navigating career development in a sales organization, etc).

If you would like to be a mentor in a certain area, please add the information to the team page. It is important to note whether you would like to be a mentor internally and/or externally at GitLab. Examples of how to specify in the expertise section of the team page: `Mentor - Marketing, Internal to GitLab` or `Mentor - Development (Ruby), External and Internal to GitLab`.

## Crew

When we work together cross functionally we call that a **crew**. A crew is a temporary group, it disbands after work is complete. A crew is self-organizing, for example our product managers are not project managers that tell you what to do, and a cross functional team does not have a manager. A crew doesn't have reporting lines, we [don't want a matrix organization](/handbook/leadership/#no-matrix-organization). An example of a crew is the people working on our project to migrate GitLab.com to Google Cloud Platform who are from the production, build, database, and Geo groups.
