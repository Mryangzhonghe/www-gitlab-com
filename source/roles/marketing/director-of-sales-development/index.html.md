---
layout: job_page
title: "Director of Sales Development"
---

## Role

As GitLab's Director of Sales Development, you will be responsible for growing demand through outbound outreach. This role will manage SDR managers worldwide to ensure our Strategic Account Leaders (field sales) have enough opportunities to meet or exceed their enterprise sales targets.

## Responsibilities

- Ensure sales accepted opportunities are sourced in accordance with company targets, and that our Strategic Account Leaders (field sales) have enough opportunities to work with to be fully productive.
- Motivate SDRs to exceed goals through coaching and incentives.
- Plan, forecast, and understand ramp adjusted capacity to ensure the team is grown effectively in tandem with the needs of our sales organization.
- Manage SDR roster for tracking ramp adjusted capacity and productivity.
- Manage SDR reports and dashboards to ensure the SDR function and the results it delivers can be easily understood by stakeholders throughout the organization.
- Build a word-class sales development team. Recruit, train, and develop a global team of SDRs and SDR managers.
- Develop paths for career advancement within the SDR function as well as to closing sales roles.
- Partner with regional sales directors and field marketing to execute cross-functional enterprise demand generation in key accounts for our field sales team.
- Work closely with marketing, sales, and people ops to ensure SDR onboarding program and ongoing training is up-to-date on our current product offering.
- Work closely with marketing, sales, and people ops to ensure SDR manager onboarding program and ongoing training is up-to-date on our current product offering.
- Partner with sales and marketing operations to ensure the SDR team has the best tools to do their job, and that they are configured to ensure SDR efficiency and productivity, especially salesforce.com and Outreach.
- Analyze Outreach cadences with an eye towards continual improvement and up-to-date messaging, by buyer persona and by industry.


## Requirements

- Proven track record of delivering sales pipeline at large enterprise accounts through leading outbound prospecting teams.
- Power user of salesforce.com and SDR cadence management software.
- Ability to drive cross functional alignment and coordination across sales and marketing teams.
- Ability to attract, retain, and motivate exceptional SDRs and SDR managers.
- Have a general understanding of Git, GitLab, and modern development practices.
- A broad knowledge of the application development ecosystem.
- Awareness of industry trends in enterprise digital transformation, devops, and continuous integration.
- Excellent written and spoken English.
- Accurate, nuanced, direct, and kind messaging.
- Being able to work independent and respond quickly.
- Able to articulate the GitLab mission, values, and vision.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Manager of Sales and Business Development.
* A 45 minute interview with our Senior Director, Marketing and Sales Development
* A 45 minute interview with our VP of Sales
* A 45 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant links

- [Sales Development Handbook](/handbook/marketing/marketing-sales-development/sdr)
